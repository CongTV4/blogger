import React from "react";
import { AppBar, makeStyles, Toolbar, Button, Hidden, IconButton, Drawer } from "@material-ui/core";
import {
    container,
    primaryColor,
    infoColor,
    successColor,
    warningColor,
    dangerColor,
    roseColor,
    drawerWidth,
    boxShadow,
    transition,
    defaultFont,
} from "assets/styles/kit";
import classNames from "classnames";
import Menu from "@material-ui/icons/Menu";

const useStyles = makeStyles({
    appBar: {
        display: "flex",
        border: "0",
        borderRadius: "3px",
        padding: "0.625rem 0",
        marginBottom: "20px",
        color: "inherit",
        width: "100%",
        backgroundColor: "transparent",
        boxShadow: "0 4px 18px 0px rgba(0, 0, 0, 0.12), 0 7px 10px -5px rgba(0, 0, 0, 0.15)",
        transition: "all 150ms ease 0s",
        alignItems: "center",
        flexFlow: "row nowrap",
        justifyContent: "flex-start",
        position: "relative",
        zIndex: "unset",
    },
    absolute: {
        position: "absolute",
        zIndex: 1100,
    },
    fixed: {
        position: "fixed",
        zIndex: 1100,
    },
    container: {
        ...container,
        minHeight: "50px",
        flex: "1",
        alignItems: "center",
        justifyContent: "space-between",
        display: "flex",
        flexWrap: "nowrap",
    },
    flex: {
        flex: 1,
    },
    title: {
        ...defaultFont,
        lineHeight: "30px",
        fontSize: "18px",
        borderRadius: "3px",
        textTransform: "none",
        color: "inherit",
        padding: "8px 16px",
        letterSpacing: "unset",
        "&:hover,&:focus": {
            color: "inherit",
            background: "transparent",
        },
    },
    appResponsive: {
        margin: "20px 10px",
        fontSize: "15px",
        "& ul": {
            "& li": {
                width: "100%",
                listStyle: "",
            },
        },
    },
    primary: {
        backgroundColor: primaryColor,
        color: "#FFFFFF",
        boxShadow: "0 4px 20px 0px rgba(0, 0, 0, 0.14), 0 7px 12px -5px rgba(156, 39, 176, 0.46)",
    },
    info: {
        backgroundColor: infoColor,
        color: "#FFFFFF",
        boxShadow: "0 4px 20px 0px rgba(0, 0, 0, 0.14), 0 7px 12px -5px rgba(0, 188, 212, 0.46)",
    },
    success: {
        backgroundColor: successColor,
        color: "#FFFFFF",
        boxShadow: "0 4px 20px 0px rgba(0, 0, 0, 0.14), 0 7px 12px -5px rgba(76, 175, 80, 0.46)",
    },
    warning: {
        backgroundColor: warningColor,
        color: "#FFFFFF",
        boxShadow: "0 4px 20px 0px rgba(0, 0, 0, 0.14), 0 7px 12px -5px rgba(255, 152, 0, 0.46)",
    },
    danger: {
        backgroundColor: dangerColor,
        color: "#FFFFFF",
        boxShadow: "0 4px 20px 0px rgba(0, 0, 0, 0.14), 0 7px 12px -5px rgba(244, 67, 54, 0.46)",
    },
    rose: {
        backgroundColor: roseColor,
        color: "#FFFFFF",
        boxShadow: "0 4px 20px 0px rgba(0, 0, 0, 0.14), 0 7px 12px -5px rgba(233, 30, 99, 0.46)",
    },
    transparent: {
        backgroundColor: "transparent !important",
        paddingTop: "20px",
        color: "#FFFFFF",
        boxShadow: "0 4px 18px 0px rgba(0, 0, 0, 0.12), 0 7px 10px -5px rgba(0, 0, 0, 0.15)",
    },
    dark: {
        color: "#FFFFFF",
        backgroundColor: "#212121 !important",
        boxShadow: "0 4px 20px 0px rgba(0, 0, 0, 0.14), 0 7px 12px -5px rgba(33, 33, 33, 0.46)",
    },
    white: {
        border: "0",
        padding: "0.625rem 0",
        marginBottom: "20px",
        color: "#555",
        backgroundColor: "#fff !important",
        boxShadow: "0 4px 18px 0px rgba(0, 0, 0, 0.12), 0 7px 10px -5px rgba(0, 0, 0, 0.15)",
    },
    drawerPaper: {
        border: "none",
        bottom: "0",
        transitionProperty: "top, bottom, width",
        transitionDuration: ".2s, .2s, .35s",
        transitionTimingFunction: "linear, linear, ease",
        width: drawerWidth,
        ...boxShadow,
        position: "fixed",
        display: "block",
        top: "0",
        height: "100vh",
        right: "0",
        left: "auto",
        visibility: "visible",
        overflowY: "visible",
        borderTop: "none",
        textAlign: "left",
        paddingRight: "0px",
        paddingLeft: "0",
        ...transition,
    },
});

type Colors = "primary" | "info" | "success" | "warning" | "danger" | "transparent" | "white" | "rose" | "dark";

interface Props {
    rightLinks: React.ReactNode;
    leftLinks?: React.ReactNode;
    brand: string;
    fixed: boolean;
    absolute: boolean;
    color: Colors;
    changeColorOnScroll: { height: number; color: Colors };
    hiddenMenu?: boolean;
}

const LayoutHeader = (props: Props) => {
    const { rightLinks, leftLinks, brand, fixed, absolute, color, hiddenMenu = false } = props;
    const classes = useStyles();
    const [mobileOpen, setMobileOpen] = React.useState(false);

    const headerColorChange = () => {
        const { color, changeColorOnScroll = { height: 0, color: "primary" } } = props;
        const windowsScrollTop = window.pageYOffset;
        if (windowsScrollTop > changeColorOnScroll.height) {
            document.body.getElementsByTagName("header")[0].classList.remove(classes[color]);
            document.body.getElementsByTagName("header")[0].classList.add(classes[changeColorOnScroll.color]);
        } else {
            document.body.getElementsByTagName("header")[0].classList.add(classes[color]);
            document.body.getElementsByTagName("header")[0].classList.remove(classes[changeColorOnScroll.color]);
        }
    };

    React.useEffect(() => {
        if (props.changeColorOnScroll) {
            window.addEventListener("scroll", headerColorChange);
        }

        return function cleanup() {
            if (props.changeColorOnScroll) {
                window.removeEventListener("scroll", headerColorChange);
            }
        };
    });

    const appBarClasses = classNames({
        [classes.appBar]: true,
        [classes[color]]: color,
        [classes.absolute]: absolute,
        [classes.fixed]: fixed,
    });

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };

    const brandComponent = <Button className={classes.title}>{brand}</Button>;

    return (
        <AppBar className={appBarClasses}>
            <Toolbar className={classes.container}>
                {leftLinks !== undefined ? brandComponent : null}
                <div className={classes.flex}>
                    {leftLinks !== undefined ? (
                        <Hidden smDown implementation="css">
                            {leftLinks}
                        </Hidden>
                    ) : (
                        brandComponent
                    )}
                </div>
                <Hidden smDown implementation="css">
                    {rightLinks}
                </Hidden>
                {!hiddenMenu && (
                    <Hidden mdUp>
                        <IconButton color="inherit" aria-label="open drawer" onClick={handleDrawerToggle}>
                            <Menu />
                        </IconButton>
                    </Hidden>
                )}
            </Toolbar>
            <Hidden mdUp implementation="js">
                <Drawer
                    variant="temporary"
                    anchor={"right"}
                    open={mobileOpen}
                    classes={{
                        paper: classes.drawerPaper,
                    }}
                    onClose={handleDrawerToggle}
                >
                    <div className={classes.appResponsive}>
                        {leftLinks}
                        {rightLinks}
                    </div>
                </Drawer>
            </Hidden>
        </AppBar>
    );
};

export default LayoutHeader;

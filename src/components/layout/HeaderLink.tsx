import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Tooltip from "@material-ui/core/Tooltip";
import { defaultFont } from "assets/styles/kit";
import Button from "../custom/Button";
import Dropdown from "components/custom/Dropdown";
import classNames from "classnames";
import profileImage from "assets/images/avatar.jpg";
import { routePath } from "routes";
import { navHistory } from "App";

const useStyles = makeStyles({
    list: {
        ...defaultFont,
        fontSize: "14px",
        margin: 0,
        paddingLeft: "0",
        listStyle: "none",
        paddingTop: "0",
        paddingBottom: "0",
        color: "inherit",
    },
    listItem: {
        float: "left",
        color: "inherit",
        position: "relative",
        display: "block",
        width: "auto",
        margin: "0",
        padding: "0",
    },
    listItemText: {
        padding: "0 !important",
    },
    navLink: {
        color: "inherit",
        position: "relative",
        padding: "0.9375rem",
        fontWeight: 400,
        fontSize: "12px",
        textTransform: "uppercase",
        borderRadius: "3px",
        lineHeight: "20px",
        textDecoration: "none",
        margin: "0px",
        display: "inline-flex",
        "&:hover,&:focus": {
            color: "inherit",
            background: "rgba(200, 200, 200, 0.2)",
        },
    },
    customDropwdown: {
        width: "57px",
        height: "57px",
        "& .material-icons": {
            margin: 0,
            fontSize: "20px",
        },
    },
    notificationNavLink: {
        color: "inherit",
        padding: "0.9375rem",
        fontWeight: 400,
        fontSize: "12px",
        textTransform: "uppercase",
        lineHeight: "20px",
        textDecoration: "none",
        margin: "0px",
        display: "inline-flex",
        top: "4px",
    },
    registerNavLink: {
        top: "3px",
        position: "relative",
        fontWeight: 400,
        fontSize: "12px",
        textTransform: "uppercase",
        lineHeight: "20px",
        textDecoration: "none",
        margin: "0px",
        display: "inline-flex",
    },
    navLinkActive: {
        color: "inherit",
        backgroundColor: "rgba(255, 255, 255, 0.1)",
    },
    icons: {
        width: "20px",
        height: "20px",
        marginRight: "3px",
    },
    socialIcons: {
        position: "relative",
        fontSize: "20px !important",
        marginRight: "4px",
    },
    dropdownLink: {
        "&,&:hover,&:focus": {
            color: "inherit",
            textDecoration: "none",
            display: "block",
            padding: "10px 20px",
        },
    },
    marginRight5: {
        marginRight: "5px",
    },
    tooltip: {
        padding: "10px 15px",
        minWidth: "130px",
        color: "#555555",
        lineHeight: "1.7em",
        background: "#FFFFFF",
        border: "none",
        borderRadius: "3px",
        boxShadow:
            "0 8px 10px 1px rgba(0, 0, 0, 0.14), 0 3px 14px 2px rgba(0, 0, 0, 0.12), 0 5px 5px -3px rgba(0, 0, 0, 0.2)",
        maxWidth: "200px",
        textAlign: "center",
        fontFamily: '"Helvetica Neue",Helvetica,Arial,sans-serif',
        fontSize: "0.875em",
        fontStyle: "normal",
        fontWeight: 400,
        textShadow: "none",
        textTransform: "none",
        letterSpacing: "normal",
        wordBreak: "normal",
        wordSpacing: "normal",
        wordWrap: "normal",
        whiteSpace: "normal",
    },
    imageDropdownButton: {
        padding: "0px",
        top: "4px",
        borderRadius: "50%",
        marginLeft: "5px",
    },
    img: {
        width: "40px",
        height: "40px",
        borderRadius: "50%",
    },
});

const HeaderLink = () => {
    const classes = useStyles();

    const btnClasses = classNames({
        [classes.navLink]: true,
        [classes.customDropwdown]: true,
    });

    const handleClick = () => {
        localStorage.clear();
        navHistory.push(routePath.Signin);
    };

    return (
        <List className={classes.list}>
            <ListItem className={classes.listItem}>
                <Tooltip
                    id="instagram-twitter"
                    title="Follow us on twitter"
                    placement={window.innerWidth > 959 ? "top" : "left"}
                    classes={{ tooltip: classes.tooltip }}
                >
                    <Button
                        color="transparent"
                        href="#"
                        target="_blank"
                        className={classes.navLink}
                        size="lg"
                        justIcon={true}
                    >
                        <i className={classes.socialIcons + " fab fa-twitter"} />
                    </Button>
                </Tooltip>
            </ListItem>
            <ListItem className={classes.listItem}>
                <Tooltip
                    id="instagram-facebook"
                    title="Follow us on facebook"
                    placement={window.innerWidth > 959 ? "top" : "left"}
                    classes={{ tooltip: classes.tooltip }}
                >
                    <Button
                        color="transparent"
                        href="#"
                        target="_blank"
                        className={btnClasses}
                        size="lg"
                        justIcon={true}
                    >
                        <i className={classes.socialIcons + " fab fa-facebook"} />
                    </Button>
                </Tooltip>
            </ListItem>
            <ListItem className={classes.listItem}>
                <Tooltip
                    id="instagram-tooltip"
                    title="Follow us on instagram"
                    placement={window.innerWidth > 959 ? "top" : "left"}
                    classes={{ tooltip: classes.tooltip }}
                >
                    <Button
                        color="transparent"
                        href="#"
                        target="_blank"
                        className={classes.navLink}
                        size="lg"
                        justIcon={true}
                    >
                        <i className={classes.socialIcons + " fab fa-instagram"} />
                    </Button>
                </Tooltip>
            </ListItem>
            <ListItem className={classes.listItem}>
                <Dropdown
                    left
                    caret={false}
                    hoverColor="black"
                    buttonText={<img src={profileImage} className={classes.img} alt="profile" />}
                    buttonProps={{
                        className: classes.navLink + " " + classes.imageDropdownButton,
                        color: "transparent",
                    }}
                    dropdownList={[{ title: "Sign Out", value: "SIGN_OUT" }]}
                    onClick={handleClick}
                />
            </ListItem>
        </List>
    );
};

export default HeaderLink;

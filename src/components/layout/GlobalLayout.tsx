import React from "react";
import LayoutHeader from "./LayoutHeader";
import HeaderLink from "./HeaderLink";
import Parallax from "./Parallax";
import Banner from "assets/images/bg2.jpg";
import GridContainer from "components/grids/GridContainer";
import GridItem from "components/grids/GridItem";
import { makeStyles } from "@material-ui/core";
import { container } from "assets/styles/kit";
import classNames from "classnames";

const useStyles = makeStyles({
    container,
    brand: {
        color: "#FFFFFF",
        textAlign: "left",
    },
    title: {
        fontSize: "2.7rem",
        fontWeight: 600,
        display: "inline-block",
        position: "relative",
    },
    subtitle: {
        fontSize: "1.313rem",
        maxWidth: "500px",
        margin: "10px 0 0",
    },
    main: {
        background: "#f5f2f1",
        position: "relative",
        zIndex: 3,
        borderRadius: "6px",
    },
    mainRaised: {
        margin: "-60px 0px 0px",
        borderRadius: "6px",
        boxShadow:
            "0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)",
        "@media (max-width: 375px)": {
            margin: "-60px 0px 0px",
        },
    },
    link: {
        textDecoration: "none",
    },
    textCenter: {
        textAlign: "center",
    },
    content: {
        borderRadius: "6px",
    },
});

const GlobalLayout: React.FunctionComponent = ({ children }) => {
    const classes = useStyles();

    return (
        <div>
            <LayoutHeader
                brand="Memories"
                rightLinks={<HeaderLink />}
                fixed={true}
                absolute={false}
                color="transparent"
                changeColorOnScroll={{
                    height: 100,
                    color: "white",
                }}
            />
            <Parallax image={Banner}>
                <div className={classes.container}>
                    <GridContainer>
                        <GridItem>
                            <div className={classes.brand}>
                                <h1 className={classes.title}>WRAP KIT.</h1>
                                <h3 className={classes.subtitle}>Enjoy the little things!!!</h3>
                            </div>
                        </GridItem>
                    </GridContainer>
                </div>
            </Parallax>
            <div className={classNames(classes.main, classes.mainRaised)}>
                <div className={classes.content}>{children}</div>
            </div>
        </div>
    );
};

export default GlobalLayout;

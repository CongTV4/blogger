import React from "react";
import { CardMedia, createStyles, makeStyles, Theme } from "@material-ui/core";
import Image from "assets/images/img2.jpg";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: "flex",
            height: "500px",

            "@media (max-width: 768px)": {
                height: "300px",
            },
            "@media (max-width: 500px)": {
                height: "150px",
            },
            borderRadius: "6px",
        },
        details: {
            display: "flex",
            flexDirection: "column",
            backgroundColor: "#fff",
        },
        content: {
            flex: "1 0 auto",
        },
        wrapperImg: {
            width: "50%",
            padding: "5px",
            boxSizing: "border-box",
            height: "100%",
            backgroundColor: "#fff",
        },
        img: {
            backgroundSize: "contain",
            height: "100%",
        },
        controls: {
            display: "flex",
            alignItems: "center",
            paddingLeft: theme.spacing(1),
            paddingBottom: theme.spacing(1),
        },
        playIcon: {
            height: 38,
            width: 38,
        },
    }),
);

// interface Props {
//     source?: string;
// }

const CardItem = () => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <div className={classes.wrapperImg}>
                <CardMedia className={classes.img} image={Image} title="Live from space album cover" />
            </div>
            <div className={classes.details}>hello</div>
        </div>
    );
};

export default CardItem;

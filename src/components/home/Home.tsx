import React from "react";
import GlobalLayout from "components/layout/GlobalLayout";
// import { Card, CardMedia, makeStyles } from "@material-ui/core";
import { makeStyles } from "@material-ui/core";
import classNames from "classnames";
import GridContainer from "components/grids/GridContainer";
import GridItem from "components/grids/GridItem";
import Banner01 from "assets/banners/banner05.jpg";
// import BG1 from "assets/bg/bg2.jpg";
// import BG6 from "assets/bg/bg6.jpg";
// import BG8 from "assets/bg/bg8.jpg";
// import BG9 from "assets/bg/bg9.jpg";
// import BG10 from "assets/bg/bg10.jpg";
// import BG11 from "assets/bg/bg11.jpg";
import Thumb from "assets/images/shape.png";

const useStyles = makeStyles({
    root: {
        height: "100vh",
    },
    section: {
        padding: "15px",
        backgroundImage: "url(" + Banner01 + ")",
        backgroundSize: "cover",
        backgroundPosition: "center center",
        paddingBottom: "100px",
        paddingTop: "50px",
    },
    section2: {
        padding: "15px",
        paddingBottom: "100px",
        paddingTop: "50px",
    },
    textCenter: {
        textAlign: "center",
    },
    h1: {
        fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
        fontSize: "3.3125rem",
        lineHeight: " 1.15em",
        fontWeight: 300,
        color: "#3c4858",
    },
    h4: {
        fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
        fontSize: "1.5625rem",
        lineHeight: " 1.4em",
        fontWeight: 300,
        color: "#3c4858",
        paddingTop: "50px",
    },
    section1: {
        display: "flex",
        justifyContent: "space-around",
        alignItems: "center",
    },
    avatar: {
        backgroundColor: "red",
    },
    media: {
        height: 0,
        paddingTop: "80%", // 16:9 56.25%
    },
    media2: {
        height: 0,
        paddingTop: "56.25%", // 16:9 56.25%
    },
    rootCard: {
        boxShadow: "none",
        marginTop: "50px",
        padding: "10px",
    },
    rotate: {
        transform: "rotate(-10deg)",
        transition: "transform .2s",
        "&:hover": {
            transform: "rotate(-10deg) scale(1.5)",
            zIndex: 10,
        },
    },
    rotate2: {
        transform: "rotate(40deg)",
    },
    thumbLeft: {
        padding: "15px",
        boxShadow: "0px 2px 16.8px 3.2px rgb(0 0 0 / 8%)",
        background: "#fff",
        width: "465px",
        height: "600px",
        boxSizing: "border-box",
    },
    thumbbg: {
        padding: "20px",
        background: "#86a0b6",
        height: "100%",
        boxSizing: "border-box",
    },
    thumbOver: {
        position: "absolute",
        left: 0,
        top: 0,
        transition: "all 5s",
        transform: "rotate(10deg)",
    },
    thumbWrap: {
        position: "relative",
    },
    thumbShape: {
        position: "absolute",
        left: "-80%",
        bottom: 0,
        zIndex: -1,
    },
});

const Home = () => {
    const classes = useStyles();

    return (
        <GlobalLayout>
            <div className={classes.root}>
                {/* <div className={classes.section}>
                    <h1 className={classNames(classes.textCenter, classes.h1)}>The Life of Material Kit</h1>
                    <h4 className={classNames(classes.textCenter, classes.h4)}>
                        It was a Thursday, but it felt like a Monday to John. And John loved Mondays. He thrived at
                        work.
                    </h4>
                    <GridContainer justifyContent="center">
                        <GridItem xs={12} sm={6} md={4} lg={3} className={classes.rotate}>
                            <Card className={classNames(classes.rootCard)}>
                                <CardMedia className={classes.media} image={BG1} title="Em <3" />
                            </Card>
                        </GridItem>
                        <GridItem xs={12} sm={6} md={4} lg={3} className={classes.rotate}>
                            <Card className={classNames(classes.rootCard)}>
                                <CardMedia className={classes.media} image={BG6} title="Em <3" />
                            </Card>
                        </GridItem>
                        <GridItem xs={12} sm={6} md={4} lg={3} className={classes.rotate}>
                            <Card className={classNames(classes.rootCard)}>
                                <CardMedia className={classes.media} image={BG8} title="Em <3" />
                            </Card>
                        </GridItem>
                    </GridContainer>
                    <GridContainer justifyContent="center">
                        <GridItem xs={12} sm={6} md={4} lg={3} className={classes.rotate}>
                            <Card className={classNames(classes.rootCard)}>
                                <CardMedia className={classes.media} image={BG9} title="Em <3" />
                            </Card>
                        </GridItem>
                        <GridItem xs={12} sm={6} md={4} lg={3} className={classes.rotate}>
                            <Card className={classNames(classes.rootCard)}>
                                <CardMedia className={classes.media} image={BG10} title="Em <3" />
                            </Card>
                        </GridItem>
                        <GridItem xs={12} sm={6} md={4} lg={3} className={classes.rotate}>
                            <Card className={classNames(classes.rootCard)}>
                                <CardMedia className={classes.media} image={BG11} title="Em <3" />
                            </Card>
                        </GridItem>
                    </GridContainer>
                </div> */}
                <div className={classes.section2}>
                    <h1 className={classNames(classes.textCenter, classes.h1)}>The Life of Material Kit</h1>

                    <GridContainer justifyContent="center">
                        <GridItem xs={10} sm={12} md={3} lg={3}>
                            <div className={classes.thumbWrap}>
                                <div className={classes.thumbLeft}>
                                    <div className={classes.thumbbg}></div>
                                </div>
                                <div className={classNames(classes.thumbLeft, classes.thumbOver)}>
                                    <div className={classes.thumbbg}></div>
                                </div>
                                <div className={classes.thumbShape}>
                                    <img src={Thumb} />
                                </div>
                            </div>
                        </GridItem>
                        <GridItem xs={12} sm={12} md={6} lg={6}></GridItem>
                    </GridContainer>
                </div>
            </div>
        </GlobalLayout>
    );
};

export default Home;

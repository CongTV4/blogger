import { Button } from "@material-ui/core";
import { createStyles, WithStyles, withStyles } from "@material-ui/core/styles";
import React from "react";
import { routePath } from "routes";
import TransitionsDialog from "./TransitionsDialog";

const styles = createStyles({
    body: {
        width: "calc(100% - 2rem)",
        height: "calc(100% - 23rem)",
        minHeight: "5rem",
        position: "relative",
        fontSize: "1.5rem",
        lineHeight: "1.8",
        wordWrap: "break-word",
        textAlign: "center",
        marginTop: "3rem",
        padding: "0 1rem",
        whiteSpace: "pre-wrap",
    },
});

interface State {
    show: boolean;
    errorCode?: number | string;
    message?: string;
}

interface PromiseInfo {
    resolve?: (value: unknown) => void;
}

declare global {
    interface Window {
        dialog: {
            show(errorCode: number | string, customMessage?: string): Promise<unknown>;
        };
    }
}

interface Props extends WithStyles<typeof styles> {}

class Dialog extends React.PureComponent<Props, State> {
    constructor(props: Props) {
        super(props);
        window.dialog = this;
        this.state = {
            show: false,
        };
    }
    promiseInfo: PromiseInfo = {};
    currentTask = new Promise((resolve) => {
        resolve("init");
    });

    show = (errorCode: number | string, message = "") => {
        if (this.state.show) {
            return this.currentTask;
        }

        return (this.currentTask = new Promise((resolve) => {
            this.promiseInfo = {
                resolve,
            };
            this.setState({
                show: true,
                errorCode,
                message: message,
            });
        }));
    };

    handleAction = () => {
        this.setState({
            show: false,
            message: "",
        });
        this.promiseInfo.resolve?.(true);
    };

    handleClose = () => {
        this.setState({
            show: false,
            message: "",
        });
        this.promiseInfo.resolve?.(false);
    };

    handleLogin = () => {
        localStorage.clear();
        this.handleClose();
        setTimeout(() => {
            window.location.href = routePath.Signin;
        });
    };

    renderAction = () => {
        switch (this.state.errorCode) {
            case 401:
                return (
                    <Button onClick={this.handleLogin} variant="outlined" color={"primary"}>
                        Go to Login page
                    </Button>
                );
            default:
                return undefined;
        }
    };

    render() {
        const { show } = this.state;

        return (
            <TransitionsDialog
                open={show}
                onAction={this.handleAction}
                onClose={this.handleClose}
                actionBtn={this.renderAction()}
                message={this.state.message}
            />
        );
    }
}

export default withStyles(styles)(Dialog);

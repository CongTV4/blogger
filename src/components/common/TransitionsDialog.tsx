import React from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import { Modal, Backdrop, Fade, Button } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        modal: {
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
        },
        paper: {
            backgroundColor: theme.palette.background.paper,
            border: "2px solid #3f51b5",
            boxShadow: theme.shadows[5],
            padding: theme.spacing(2, 4, 3),
            minWidth: "300px",
        },
        title: {
            fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
            fontSize: "1.2rem",
            lineHeight: " 1.4em",
            fontWeight: 500,
        },
        message: {
            fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
            fontSize: "1.1rem",
            lineHeight: " 1.4em",
            fontWeight: 300,
        },
        footer: {
            display: "flex",
            justifyContent: "center",
        },
    }),
);

interface Props {
    title?: string | React.ReactNode;
    message?: React.ReactNode | string;
    actionBtn?: React.ReactNode;
    open: boolean;
    onAction?: () => void;
    onClose: () => void;
}

export default function TransitionsDialog({
    open,
    onClose,
    onAction,
    title = "Transaction Error",
    message,
    actionBtn,
}: Props) {
    const classes = useStyles();

    return (
        <div>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open}
                onClose={onClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div className={classes.paper}>
                        {title && (
                            <h2 className={classes.title} id="transition-modal-title">
                                {title}
                            </h2>
                        )}
                        {message && (
                            <p id="transition-modal-description" className={classes.message}>
                                {message}
                            </p>
                        )}
                        <div className={classes.footer}>
                            {actionBtn ? actionBtn : <Button onClick={onAction}>Close</Button>}
                        </div>
                    </div>
                </Fade>
            </Modal>
        </div>
    );
}

/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState } from "react";
import Header from "components/layout/LayoutHeader";
import { Card, CardContent, makeStyles, InputAdornment, Button, Link } from "@material-ui/core";
import { container } from "assets/styles/kit";
import Banner from "assets/images/bg7.jpg";
import GridContainer from "components/grids/GridContainer";
import GridItem from "components/grids/GridItem";
import classNames from "classnames";
import CustomInput from "components/custom/CustomInput";
import People from "@material-ui/icons/People";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import { routePath } from "routes";
import { signin } from "services/authen/authen";
import axios from "axios";
import { parseJwt } from "utils/utils";
import { useAuth } from "../../hook/authHook";
import { navHistory } from "../../App";

const useStyles = makeStyles({
    container: {
        ...container,
        zIndex: 2,
        position: "relative",
        paddingTop: "20vh",
        color: "#FFFFFF",
        paddingBottom: "200px",
        boxSizing: "border-box",
    },
    cardHidden: {
        opacity: "0",
        transform: "translate3d(0, -60px, 0)",
    },
    pageHeader: {
        minHeight: "100vh",
        height: "auto",
        display: "inherit",
        position: "relative",
        margin: "0",
        padding: "0",
        border: "0",
        alignItems: "center",
        "&:before": {
            background: "rgba(0, 0, 0, 0.5)",
        },
        "&:before,&:after": {
            position: "absolute",
            zIndex: "1",
            width: "100%",
            height: "100%",
            display: "block",
            left: "0",
            top: "0",
            content: '""',
        },
        "& footer li a,& footer li a:hover,& footer li a:active": {
            color: "#FFFFFF",
        },
        "& footer": {
            position: "absolute",
            bottom: "0",
            width: "100%",
        },
    },
    form: {
        margin: "0",
    },
    cardHeader: {
        width: "auto",
        textAlign: "center",
        marginLeft: "20px",
        marginRight: "20px",
        marginTop: "-40px",
        padding: "20px 0",
        marginBottom: "15px",
    },
    socialIcons: {
        maxWidth: "24px",
        marginTop: "0",
        width: "100%",
        transform: "none",
        left: "0",
        top: "0",
        height: "100%",
        lineHeight: "41px",
        fontSize: "20px",
    },
    divider: {
        marginTop: "30px",
        marginBottom: "0px",
        textAlign: "center",
        fontSize: "14px",
        margin: " 0 0 10px",
    },
    cardFooter: {
        paddingTop: "0rem",
        border: "0",
        borderRadius: "6px",
        justifyContent: "center !important",
    },
    socialLine: {
        marginTop: "1rem",
        textAlign: "center",
        padding: "0",
    },
    inputIconsColor: {
        color: "#495057",
    },
    cardContentWrapper: {
        textAlign: "center",
        paddingLeft: "30px",
        paddingRight: "30px",
        width: "285px",
        margin: "0 auto",
        "@media(max-width: 370px)": {
            width: "auto",
        },
    },
    none: {},
    title: {
        marginTop: "15px",
        marginBottom: "0px",
        textAlign: "center",
        fontSize: "20px",
        margin: " 0 0 10px",
    },
    social: {
        width: "100%",
        paddingTop: 0,
        paddingBottom: 0,
    },
    btnContent: {
        width: "100%",
        display: "flex",
        alignItems: "center",
    },
    socialTitle: {
        width: "calc(100% - 20px)",
    },
    socialWrapper: {
        display: "flex",
        flexDirection: "column",
        gap: "10px 0",
        paddingTop: "27px",
    },
    fullWidth: {
        width: "100%",
        boxShadow: "none",
        padding: "10px 0",
        marginBottom: "7px",
    },
    regisLink: {
        textAlign: "left",
        fontSize: "12px",
    },
});

const Login = (props: any) => {
    const { userInfo } = useAuth();
    const cts = axios.CancelToken.source();
    const [loginInfo, setLoginInfo] = useState({ username: "", password: "" });
    const classes = useStyles();
    const { ...rest } = props;

    const mapClasses = classNames({
        [classes.cardContentWrapper]: true,
    });

    const handleChangeValue = (type: "username" | "password") => (e: any) => {
        const newInfo = { ...loginInfo, [type]: e.target?.value || "" };
        setLoginInfo(newInfo);
    };

    const handleSubmit = async () => {
        const data = await signin({ email: loginInfo.username, password: loginInfo.password }, cts.token);
        const payload = parseJwt(data?.token || "");
        if (payload) {
            localStorage.setItem("access_token", data?.token || "");
            localStorage.setItem("user_id", String(payload.id));

            window.location.href = routePath.Home;
        }
    };

    useEffect(() => {
        if (userInfo) navHistory.push(routePath.Home);
    }, [userInfo]);

    return (
        <>
            <Header absolute color="transparent" brand="Memories" hiddenMenu={true} {...rest} />
            <div
                className={classes.pageHeader}
                style={{
                    backgroundImage: "url(" + Banner + ")",
                    backgroundSize: "cover",
                    backgroundPosition: "top center",
                }}
            >
                <div className={classes.container}>
                    <GridContainer justifyContent="center">
                        <GridItem xs={12} sm={12} md={4}>
                            <form>
                                <Card className={mapClasses}>
                                    <CardContent>
                                        <p className={classes.title}>Log in to your account</p>

                                        <div className={classes.socialWrapper}>
                                            <Button variant="outlined" color="secondary" className={classes.social}>
                                                <div className={classes.btnContent}>
                                                    <i className={classes.socialIcons + " fab fa-google"} />{" "}
                                                    <span className={classes.socialTitle}>Login with Google</span>
                                                </div>
                                            </Button>

                                            <Button variant="outlined" color="primary" className={classes.social}>
                                                <div className={classes.btnContent}>
                                                    <i className={classes.socialIcons + " fab fa-facebook"} />{" "}
                                                    <span className={classes.socialTitle}>Login with Facebook</span>
                                                </div>
                                            </Button>
                                        </div>
                                        <p className={classes.divider}>Or Be Classical</p>
                                        <CustomInput
                                            labelText="Username..."
                                            id="first"
                                            formControlProps={{
                                                fullWidth: true,
                                            }}
                                            inputProps={{
                                                type: "text",
                                                endAdornment: (
                                                    <InputAdornment position="end">
                                                        <People className={classes.inputIconsColor} />
                                                    </InputAdornment>
                                                ),
                                                onChange: handleChangeValue("username"),
                                            }}
                                        />
                                        <CustomInput
                                            labelText="Password"
                                            id="pass"
                                            formControlProps={{
                                                fullWidth: true,
                                            }}
                                            inputProps={{
                                                type: "password",
                                                endAdornment: (
                                                    <InputAdornment position="end">
                                                        <LockOutlinedIcon
                                                            className={classes.inputIconsColor}
                                                        ></LockOutlinedIcon>
                                                    </InputAdornment>
                                                ),
                                                autoComplete: "off",
                                                onChange: handleChangeValue("password"),
                                            }}
                                        />
                                        <Button
                                            variant="contained"
                                            color="secondary"
                                            className={classes.fullWidth}
                                            onClick={handleSubmit}
                                        >
                                            Log in
                                        </Button>
                                        <Link href="/signup" underline="none" className={classes.regisLink}>
                                            {`Don't have account?`}
                                        </Link>
                                    </CardContent>
                                </Card>
                            </form>
                        </GridItem>
                    </GridContainer>
                </div>
            </div>
        </>
    );
};

export default Login;

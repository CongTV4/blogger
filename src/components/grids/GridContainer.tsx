import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid, { GridJustification, GridItemsAlignment } from "@material-ui/core/Grid";

const styles = {
    grid: {
        marginRight: "-15px",
        marginLeft: "-15px",
        width: "auto",
    },
};

const useStyles = makeStyles(styles);

interface Props {
    className?: string;
    justifyContent?: GridJustification;
    alignItems?: GridItemsAlignment;
    maxWidth?: string;
}

const GridContainer: React.FC<Props> = (props) => {
    const classes = useStyles();
    const { className = "", children, alignItems, justifyContent, ...rest } = props;

    return (
        <Grid
            container
            className={classes.grid + " " + className}
            justifyContent={justifyContent}
            alignItems={alignItems}
            {...rest}
        >
            {children}
        </Grid>
    );
};

export default GridContainer;

import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid, { GridSize } from "@material-ui/core/Grid";

const useStyles = makeStyles({
    grid: {
        position: "relative",
        width: "100%",
        minHeight: "1px",
        paddingRight: "15px",
        paddingLeft: "15px",
        flexBasis: "auto",
    },
});

interface Props {
    className?: string;
    xs?: boolean | GridSize;
    sm?: boolean | GridSize;
    md?: boolean | GridSize;
    lg?: boolean | GridSize;
}

const GridItem: React.FunctionComponent<Props> = (props) => {
    const classes = useStyles();

    const { children, className = "", xs, sm, md, lg, ...rest } = props;

    return (
        <Grid item {...rest} className={classes.grid + " " + className} xs={xs} md={md} sm={sm} lg={lg}>
            {children}
        </Grid>
    );
};

export default GridItem;

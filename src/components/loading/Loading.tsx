import React from "react";
import { makeStyles, createStyles } from "@material-ui/core/styles";
import { CircularProgress } from "@material-ui/core";
import { useSelector } from "react-redux";
import { RootState } from "stores";

const useStyles = makeStyles(() =>
    createStyles({
        root: {
            position: "fixed",
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            zIndex: 1200,
            backgroundColor: "transparent",
        },
        content: {
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "100vh",
            backgroundColor: "rgba(255,255,255,0.7)",
            touchAction: "none",
        },
    }),
);

interface Props {
    loading?: boolean;
}

const Loading = (props: Props) => {
    const isLoading = useSelector((state: RootState) => state.request.loading);
    const classes = useStyles();

    return isLoading || props.loading ? (
        <div className={`${classes.root}`}>
            <div className={classes.content}>
                <CircularProgress color="secondary" />
            </div>
        </div>
    ) : (
        <></>
    );
};

export default Loading;

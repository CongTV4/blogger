import { makeStyles } from "@material-ui/core";
import React from "react";
import {
    warningCardHeader,
    successCardHeader,
    dangerCardHeader,
    infoCardHeader,
    primaryCardHeader,
} from "assets/styles/kit";
import classNames from "classnames";

const useStyles = makeStyles({
    cardHeader: {
        borderRadius: "3px",
        padding: "1rem 15px",
        marginLeft: "15px",
        marginRight: "15px",
        marginTop: "-30px",
        border: "0",
        marginBottom: "0",
    },
    cardHeaderPlain: {
        marginLeft: "0px",
        marginRight: "0px",
    },
    warningCardHeader,
    successCardHeader,
    dangerCardHeader,
    infoCardHeader,
    primaryCardHeader,
});

interface Props {
    className?: string;
    color: "warning" | "success" | "danger" | "info" | "primary";
    plain?: boolean;
}

type MapColor = "warningCardHeader" | "successCardHeader" | "dangerCardHeader" | "infoCardHeader" | "primaryCardHeader";

const CardHeader: React.FunctionComponent<Props> = (props) => {
    const classes = useStyles();
    const { className, children, color, plain, ...rest } = props;
    const classColor = (color + "CardHeader") as MapColor;
    const cardHeaderClasses = classNames({
        [classes.cardHeader]: true,
        [classes[classColor]]: color,
        [classes.cardHeaderPlain]: plain,
        [className || ""]: className !== undefined,
    });

    return (
        <div className={cardHeaderClasses} {...rest}>
            {children}
        </div>
    );
};

export default CardHeader;

/* eslint-disable @typescript-eslint/no-explicit-any */
import { FormControl, InputLabel, makeStyles, Input } from "@material-ui/core";
import { dangerColor, defaultFont, primaryColor, successColor } from "assets/styles/kit";
import classNames from "classnames";
import React from "react";

const useStyles = makeStyles({
    disabled: {
        "&:before": {
            borderColor: "transparent !important",
        },
    },
    underline: {
        "&:hover:not($disabled):before,&:before": {
            borderColor: "#D2D2D2 !important",
            borderWidth: "1px !important",
        },
        "&:after": {
            borderColor: primaryColor,
        },
    },
    underlineError: {
        "&:after": {
            borderColor: dangerColor,
        },
    },
    underlineSuccess: {
        "&:after": {
            borderColor: successColor,
        },
    },
    whiteUnderline: {
        "&:hover:not($disabled):before,&:before": {
            borderColor: "#FFFFFF",
        },
        "&:after": {
            borderColor: "#FFFFFF",
        },
    },
    labelRoot: {
        ...defaultFont,
        color: "#AAAAAA !important",
        fontWeight: 400,
        fontSize: "14px",
        lineHeight: "1.42857",
        top: "10px",
        letterSpacing: "unset",
        "& + $underline": {
            marginTop: "0px",
        },
    },
    labelRootError: {
        color: dangerColor + " !important",
    },
    labelRootSuccess: {
        color: successColor + " !important",
    },
    formControl: {
        margin: "0 0 17px 0",
        paddingTop: "27px",
        position: "relative",
        "& svg,& .fab,& .far,& .fal,& .fas,& .material-icons": {
            color: "#495057",
        },
    },
    input: {
        color: "#495057",
        height: "unset",
        "&,&::placeholder": {
            fontSize: "14px",
            fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
            fontWeight: "400",
            lineHeight: "1.42857",
            opacity: "1",
        },
        "&::placeholder": {
            color: "#AAAAAA",
        },
    },
    whiteInput: {
        "&,&::placeholder": {
            color: "#FFFFFF",
            opacity: "1",
        },
    },
});

const CustomInput = (props: any) => {
    const classes = useStyles();
    const { formControlProps, labelText, id, labelProps, inputProps, error, white, inputRootCustomClasses, success } =
        props;

    const labelClasses = classNames({
        [" " + classes.labelRootError]: error,
        [" " + classes.labelRootSuccess]: success && !error,
    });
    const underlineClasses = classNames({
        [classes.underlineError]: error,
        [classes.underlineSuccess]: success && !error,
        [classes.underline]: true,
        [classes.whiteUnderline]: white,
    });
    const marginTop = classNames({
        [inputRootCustomClasses]: inputRootCustomClasses !== undefined,
    });
    const inputClasses = classNames({
        [classes.input]: true,
        [classes.whiteInput]: white,
    });
    let formControlClasses;
    if (formControlProps !== undefined) {
        formControlClasses = classNames(formControlProps.className, classes.formControl);
    } else {
        formControlClasses = classes.formControl;
    }

    return (
        <FormControl {...formControlProps} className={formControlClasses}>
            {labelText !== undefined ? (
                <InputLabel className={classes.labelRoot + " " + labelClasses} htmlFor={id} {...labelProps}>
                    {labelText}
                </InputLabel>
            ) : null}
            <Input
                classes={{
                    input: inputClasses,
                    root: marginTop,
                    disabled: classes.disabled,
                    underline: underlineClasses,
                }}
                id={id}
                {...inputProps}
            />
        </FormControl>
    );
};

export default CustomInput;

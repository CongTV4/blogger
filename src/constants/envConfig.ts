export const ENV_CONFIG_DEFAULT = Object.freeze({
    API_URL: process.env.REACT_APP_API_URL as string,
    API_TIME_OUT: 30000,
});

import { LoginResponse } from "./../interfaces/response/authResponse";
import axios from "axios";
import { useEffect, useState } from "react";
import { getUserInfo } from "../services/authen/authen";

export const useAuth = () => {
    const [userInfo, setUserInfo] = useState<LoginResponse | undefined>();
    const [isLoading, setLoading] = useState<boolean>(true);

    useEffect(() => {
        const cts = axios.CancelToken.source();
        const token = localStorage.getItem("access_token");
        const userID = localStorage.getItem("user_id");
        setLoading(true);
        async function checkUser() {
            if (token && userID) {
                const user = await getUserInfo(Number(userID), { cancelToken: cts.token, ignoreError: true });
                setUserInfo(user);
                setLoading(false);
            } else {
                setUserInfo(undefined);
                setLoading(false);
            }
        }
        checkUser();

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return {
        userInfo,
        isLoading,
    };
};

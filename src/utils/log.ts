/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
let logDev = (message?: any, ...optionalParams: any[]) => {};

if (process.env.NODE_ENV === "development") {
    logDev = console.log.bind(console);
}

export { logDev };

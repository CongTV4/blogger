export interface BaseResponse extends ServerError {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    data: any;
    message: string;
    lastUpdateTime?: string;
}

export interface ServerError {
    apiPath: string;
    errorCode: number | string;
}

export * from "./authResponse";

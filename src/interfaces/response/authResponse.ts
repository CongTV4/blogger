export interface LoginResponse {
    name: string;
    id: string;
    token: string;
}

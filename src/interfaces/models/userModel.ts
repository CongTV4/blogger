export interface UserModel {
    email: string;
    id: string;
    age?: string;
    avatar?: string;
}

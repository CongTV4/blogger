/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { Suspense, lazy, Component } from "react";
import { Redirect, Route, Router, Switch } from "react-router";
import { routePath } from "routes";
import { createBrowserHistory } from "history";
import Loading from "components/loading/Loading";
import "./assets/styles/style.css";
import Dialog from "components/common/ErrorDialog";
import { LoginResponse } from "interfaces/response";
import { getUserInfo } from "services/authen/authen";
import axios from "axios";
import { setLoading } from "stores/request/action";
import { AnyAction, bindActionCreators, Dispatch } from "redux";
import { connect } from "react-redux";
import { useAuth } from "./hook/authHook";

const HomePage = lazy(() => import("components/home/Home"));
const LoginPage = lazy(() => import("components/auth/Login"));
const SignupPage = lazy(() => import("components/auth/Register"));

export const navHistory = createBrowserHistory({
    basename: process.env.PUBLIC_URL,
});

const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) =>
    bindActionCreators(
        {
            setLoading,
        },
        dispatch,
    );

interface Props extends ReturnType<typeof mapDispatchToProps> {}

interface PrivateProps {}

const PrivateRoute: React.FC<PrivateProps> = ({ children, ...rest }) => {
    const { userInfo, isLoading } = useAuth();

    console.log("loading", isLoading);
    if (isLoading) return <Loading loading={true} />;

    return (
        <Route
            {...rest}
            render={({ location }) =>
                userInfo ? (
                    children
                ) : (
                    <Redirect
                        to={{
                            pathname: routePath.Signin,
                            state: { from: location },
                        }}
                    />
                )
            }
        />
    );
};

interface State {
    user?: LoginResponse | null;
    isLoading?: boolean;
}

class App extends Component<Props, State> {
    render() {
        return (
            <Router history={navHistory}>
                <Dialog />
                <Suspense fallback={<Loading loading={true} />}>
                    <Switch>
                        <Route path={routePath.Signin} exact component={LoginPage}></Route>
                        <Route path={routePath.Signup} exact component={SignupPage}></Route>
                        <PrivateRoute>
                            <Route path={routePath.Home} exact component={HomePage}></Route>
                        </PrivateRoute>
                    </Switch>
                </Suspense>
            </Router>
        );
    }
}

export default connect(null, mapDispatchToProps)(App);

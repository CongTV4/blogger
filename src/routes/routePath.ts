export const routePath = Object.freeze({
    Home: "/",
    About: "/about",
    Signin: "/signin",
    Signup: "/signup",
});

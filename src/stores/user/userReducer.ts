import { AnyAction } from "redux";
import { SET_USER_INFO } from "../../constants";

interface UserInfoState {
    email: string;
    id: string;
    age?: string;
    avatar?: string;
}

const initState: UserInfoState = {
    email: "",
    id: "",
    age: "",
    avatar: "",
};

const userInfoReducer = (state = initState, action: AnyAction) => {
    switch (action.type) {
        case SET_USER_INFO:
            const info = { ...action.info };

            return {
                ...info,
            };

        default:
            return state;
    }
};

export default userInfoReducer;

import { UserModel } from "../../interfaces/models";
import { SET_USER_INFO } from "../../constants/actions";

export const setUserInfo = (info: UserModel) => ({
    type: SET_USER_INFO,
    info,
});

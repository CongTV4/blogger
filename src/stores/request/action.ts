import { SET_LOADING } from "constants/actions";

export const setLoading = (showLoading: boolean) => ({
    type: SET_LOADING,
    showLoading,
});

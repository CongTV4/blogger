import { SET_LOADING } from "constants/actions";
import { AnyAction } from "redux";

interface RequestState {
    loading: boolean;
}

const initState: RequestState = {
    loading: false,
};

const apiReducer = (state = initState, action: AnyAction) => {
    switch (action.type) {
        case SET_LOADING:
            return {
                ...state,
                loading: action.showLoading as boolean,
            };
        default:
            return state;
    }
};

export default apiReducer;

import { applyMiddleware, combineReducers, createStore } from "redux";
import thunk from "redux-thunk";
// reducers
import userInfo from "./user/userReducer";
import request from "./request/requestReducer";

const rootReducer = combineReducers({
    // push reducer here
    request,
    userInfo,
});

export const store = createStore(rootReducer, applyMiddleware(thunk));

export type RootState = ReturnType<typeof rootReducer>;

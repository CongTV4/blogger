import { CancelToken } from "axios";
import { LoginRequest, RegisterRequest } from "interfaces/request";
import { LoginResponse } from "interfaces/response";
import { request, RequestConfig } from "services/request";

export const signin = async (data: LoginRequest, cancelToken?: CancelToken) => {
    const result = await request<LoginResponse>({
        method: "post",
        url: "/auth/signin",
        data,
        cancelToken,
    });

    return result && result.data ? result.data : null;
};

export const signup = async (data: RegisterRequest, cancelToken?: CancelToken) => {
    const result = await request<LoginResponse>({
        method: "post",
        url: "/auth/signup",
        data,
        cancelToken,
    });

    return result && result.data ? result.data : null;
};

export const getUserInfo = async (userId: number, options?: RequestConfig) => {
    const result = await request<LoginResponse>({
        method: "get",
        url: `/user/${userId}`,
        ...options,
    });

    return result && result.data ? result.data : undefined;
};

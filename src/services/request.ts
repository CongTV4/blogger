/* eslint-disable @typescript-eslint/no-explicit-any */
import axios, { AxiosRequestConfig, AxiosResponse } from "axios";
import { ENV_CONFIG_DEFAULT } from "constants/envConfig";
import { store } from "stores";
import { setLoading } from "stores/request/action";

let numberAPIs = 0;

export interface RequestConfig extends AxiosRequestConfig {
    // custom props
    retry?: boolean;
    ignoreError?: boolean;
}

export const request = async <T = any>(config: RequestConfig): Promise<AxiosResponse<T>> => {
    const instance = axios.create({
        baseURL: ENV_CONFIG_DEFAULT.API_URL,
        timeout: ENV_CONFIG_DEFAULT.API_TIME_OUT,
        headers: {
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
            Accept: "application/json",
            "Content-Type": "application/json",
        },
    });

    startLoading();

    instance.interceptors.response.use(
        function (response) {
            // Any status code that lie within the range of 2xx cause this function to trigger
            // Do something with response data
            stopLoading();

            return response;
        },
        async function (error) {
            const { config } = error;
            // Any status codes that falls outside the range of 2xx cause this function to trigger
            // Do something with response error
            stopLoading();
            if (config.ignoreError) {
                return error;
            }
            if (config.retry) {
                const status = await window.dialog.show(
                    error.response?.status || 500,
                    error.response?.message || "Unexpected Error",
                );
                if (status) return request(error.config);
            } else
                window.dialog.show(
                    error.response?.status || 500,
                    error.response?.message || error.response?.data?.message || "Unexpected Error",
                );

            return error;
        },
    );

    return await instance.request({ ...config });
};

const startLoading = () => {
    if (numberAPIs <= 0) store.dispatch(setLoading(true));
    numberAPIs++;
};

const stopLoading = () => {
    numberAPIs--;
    if (numberAPIs > 0) {
        return;
    }
    store.dispatch(setLoading(false));
};

import { useContext } from "react";
import { AuthProviderContext } from "./AuthProvider";

function useAuthProvider() {
    const { userInfo, onLogin, onLogout } = useContext(AuthProviderContext);

    return { userInfo, onLogin, onLogout };
}

export default useAuthProvider;

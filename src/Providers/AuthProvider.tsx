/* eslint-disable react-hooks/exhaustive-deps */
import axios from "axios";
import React, { useCallback, useState } from "react";
import { signin } from "services/authen/authen";

interface AuthContext {
    userInfo: { id: number; token: string; email: string } | null;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    onLogin: (username: string, password: string) => Promise<boolean> | Promise<any>;
    onLogout: () => void;
}

export const AuthProviderContext = React.createContext<AuthContext>({
    userInfo: null,
    onLogin: () => {
        return Promise.resolve(true);
    },
    onLogout: () => {
        // default none
    },
});

const AuthProvider: React.FunctionComponent = ({ children }) => {
    const [userInfo, setUserInfo] = useState<null | { id: number; token: string; email: string }>(null);
    const cts = axios.CancelToken.source();
    const logout = () => {
        localStorage.clear();
        setUserInfo(null);
    };

    const login = async (username: string, password: string) => {
        const data = await signin({ email: username, password }, cts.token);
        if (data) {
            setUserInfo({ id: 1, email: username, token: data?.token || "" });

            return Promise.resolve(true);
        }

        return Promise.reject(false);
    };

    const contextValue = {
        userInfo,
        onLogin: useCallback((username: string, password: string) => login(username, password), []),
        onLogout: useCallback(() => logout(), []),
    };

    return <AuthProviderContext.Provider value={contextValue}>{children}</AuthProviderContext.Provider>;
};

export default AuthProvider;
